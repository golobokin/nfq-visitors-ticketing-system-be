package lt.nfq.intern.electronicvisitorsticketingsystem.security.services;

import lombok.RequiredArgsConstructor;
import lt.nfq.intern.electronicvisitorsticketingsystem.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) {

        return UserDetailsImpl.build(userService.getUserByUserName(username));
    }
}
