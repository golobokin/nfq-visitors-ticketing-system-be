package lt.nfq.intern.electronicvisitorsticketingsystem.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lt.nfq.intern.electronicvisitorsticketingsystem.data.entity.ERole;
import lt.nfq.intern.electronicvisitorsticketingsystem.data.entity.User;
import lt.nfq.intern.electronicvisitorsticketingsystem.data.repo.UserRepository;
import lt.nfq.intern.electronicvisitorsticketingsystem.payload.response.AvailableSpecialistsResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService {
    private final UserRepository userRepository;

    public User getUserByUserName(String username) {
        log.info("Retrieving user with username {}", username);
        return userRepository.findByUsername(username).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("User with username %s not found", username)));
    }

    public List<AvailableSpecialistsResponse> getAllAvailableSpecialists() {
        log.info("Retrieving available specialists");
        return userRepository
                .findAllByRole(ERole.ROLE_SPECIALIST)
                .stream()
                .filter(user ->
                        user
                                .getShiftStartTime()
                                .atDate(LocalDate.now())
                                .isBefore(LocalDateTime.now())
                        &&
                        user
                                .getShiftEndTime()
                                .atDate(LocalDate.now())
                                .isAfter(LocalDateTime.now())
                ).collect(Collectors.toList());
    }

    public User getUserById(Long id) {
        log.info("Retrieving user with id {}", id);
        return userRepository.findByIdAndRole(id, ERole.ROLE_SPECIALIST).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Specialist with id %d not found", id)));
    }

    public void updateSpecialist(User specialist) {
        log.info("Updating specialist to database");
        userRepository.save(specialist);
    }
}
