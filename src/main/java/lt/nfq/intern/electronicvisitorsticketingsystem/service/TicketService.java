package lt.nfq.intern.electronicvisitorsticketingsystem.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lt.nfq.intern.electronicvisitorsticketingsystem.data.entity.Ticket;
import lt.nfq.intern.electronicvisitorsticketingsystem.data.entity.User;
import lt.nfq.intern.electronicvisitorsticketingsystem.data.repo.TicketRepository;
import lt.nfq.intern.electronicvisitorsticketingsystem.payload.response.TicketResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class TicketService {
    private final TicketRepository ticketRepository;
    private final UserService userService;

    public TicketResponse issueNewSpecialistTicket(Long specialistId) {
        User specialist = userService.getUserById(specialistId);
        Ticket ticket = new Ticket();
        log.info("Start issuing ticket for specialist {}", specialistId);
        ticket.setUser(specialist);

        LocalDateTime nextAvailableVisitTime = specialist.getNextTicketTime();
        LocalDateTime specialistShiftStartTime = specialist.getShiftStartTime().atDate(LocalDate.now());
        LocalDateTime specialistShiftEndTime = specialist.getShiftEndTime().atDate(LocalDate.now());

        boolean isShift = LocalDateTime.now().isAfter(specialistShiftStartTime) &&
                LocalDateTime.now().isBefore(specialistShiftEndTime) &&
                nextAvailableVisitTime.isBefore(specialistShiftEndTime);

        Long currentTicketNumber = specialist.getNextTicketNumber();
        if (nextAvailableVisitTime.isBefore(specialistShiftStartTime)) {
            currentTicketNumber = specialist.getId() * 1000;
        }

        if (isShift && nextAvailableVisitTime.isAfter(LocalDateTime.now())) {
            ticket.setVisitBookedTime(nextAvailableVisitTime);
            ticket.setTicketNumber(currentTicketNumber);
            log.info("Saving new ticket to database");
            ticketRepository.save(ticket);

            nextAvailableVisitTime = ticket.getVisitBookedTime().plusMinutes(specialist.getVisitLengthInMinutes());
            specialist.setNextTicketTime(nextAvailableVisitTime);
            specialist.setNextTicketNumber(++currentTicketNumber);
            userService.updateSpecialist(specialist);

        } else if (isShift && nextAvailableVisitTime.isBefore(LocalDateTime.now())) {
            ticket.setVisitBookedTime(LocalDateTime.now());
            ticket.setTicketNumber(currentTicketNumber);
            log.info("Saving new ticket to database");
            ticketRepository.save(ticket);

            nextAvailableVisitTime = ticket.getVisitBookedTime().plusMinutes(specialist.getVisitLengthInMinutes());
            specialist.setNextTicketTime(nextAvailableVisitTime);
            specialist.setNextTicketNumber(++currentTicketNumber);
            userService.updateSpecialist(specialist);

        } else if (!isShift) {
            log.info("Unable to issue ticket - no available visit time.");
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Unable to issue ticket - no available reservation time today for selected specialist.");
        }

        return ticketRepository.findTicketById(ticket.getId());
    }

    public Ticket getFullTicketById(Long id) {
        log.info("Retrieving ticket with id {}", id);
        return ticketRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Ticket id: %d not found.", id)));
    }

    public TicketResponse getShortTicketById(Long id) {
        log.info("Retrieving ticket with id {}", id);
        return ticketRepository.findTicketById(id);
    }

    public void cancelTicket(Long id) {
        Ticket ticket = getFullTicketById(id);
        log.info("Cancelling ticket with id {}", id);
        ticket.setCancelled(true);
        ticketRepository.save(ticket);
    }

    public void markTicketHasStarted(Long id) {
        Ticket ticket = getFullTicketById(id);
        log.info("Marking start time for ticket with id {}", id);
        ticket.setVisitStartTime(LocalDateTime.now());
        ticket.setHasTicketStarted(true);
        ticketRepository.save(ticket);
    }

    public void markTicketHasEnded(Long id) {
        Ticket ticket = getFullTicketById(id);
        log.info("Marking end time for  ticket with id {}", id);
        ticket.setVisitEndTime(LocalDateTime.now());
        ticket.setHasTicketEnded(true);
        ticketRepository.save(ticket);
    }

    public List<TicketResponse> getTodayTicketsForOneSpecialist(Long specialistId) {
        log.info("Getting next tickets for specialist with id {}", specialistId);
        LocalDateTime searchStart = LocalDateTime.now();
        LocalDateTime searchEnd = LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.MIDNIGHT);
        return ticketRepository.findAllByVisitBookedTimeBetweenAndUserId(searchStart, searchEnd, specialistId);
    }

    public TicketResponse getCurrentTicketForSpecialist(Long specialistId) {
        log.info("Getting current ticket for specialist with id {}", specialistId);
        Long visitLength = userService.getUserById(specialistId).getVisitLengthInMinutes();
        LocalDateTime searchStart = LocalDateTime.now().minusMinutes(visitLength);
        LocalDateTime searchEnd = LocalDateTime.now();
        return ticketRepository.findTicketByVisitBookedTimeBetweenAndUserId(searchStart, searchEnd, specialistId);
    }

    public List<TicketResponse> getNextFiveTicketsForSpecialist(Long specialistId) {
        log.info("Getting next 5 tickets for specialist with id {}", specialistId);
        LocalDateTime searchStart = LocalDateTime.now();
        LocalDateTime searchEnd = LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.MIDNIGHT);
        return ticketRepository.findFirst5ByVisitBookedTimeBetweenAndUserId(searchStart, searchEnd, specialistId);
    }

}
