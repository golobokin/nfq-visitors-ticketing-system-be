package lt.nfq.intern.electronicvisitorsticketingsystem.payload.response;

import java.time.LocalTime;

public interface AvailableSpecialistsResponse {
    Long getId();

    String getType();

    LocalTime getShiftStartTime();

    LocalTime getShiftEndTime();

}
