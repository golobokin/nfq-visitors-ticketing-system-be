package lt.nfq.intern.electronicvisitorsticketingsystem.payload.response;

import java.time.LocalDateTime;
import java.time.LocalTime;

public interface SpecialistResponse {
    Long getId();

    String getType();

    LocalTime getShiftStartTime();

    LocalTime getShiftEndTime();

    Long getVisitLengthInMinutes();

    LocalDateTime getNextTicketTime();

    Long getNextTicketNumber();
}
