package lt.nfq.intern.electronicvisitorsticketingsystem.payload.response;

import java.time.LocalDateTime;

public interface TicketResponse {
    Long getId();

    Long getTicketNumber();

    LocalDateTime getVisitBookedTime();

    boolean getHasTicketStarted();

    boolean getHasTicketEnded();

    boolean getIsCancelled();
}
