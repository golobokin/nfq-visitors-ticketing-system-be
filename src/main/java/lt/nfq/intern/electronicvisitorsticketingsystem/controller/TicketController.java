package lt.nfq.intern.electronicvisitorsticketingsystem.controller;

import lombok.RequiredArgsConstructor;
import lt.nfq.intern.electronicvisitorsticketingsystem.payload.response.TicketResponse;
import lt.nfq.intern.electronicvisitorsticketingsystem.service.TicketService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class TicketController {
    private final TicketService ticketService;

    @GetMapping("/public/ticket/specialist/{id}")
    public ResponseEntity<TicketResponse> issueNewTicket(@PathVariable("id") Long specialistId) {
        return ResponseEntity.ok(ticketService.issueNewSpecialistTicket(specialistId));
    }

    @GetMapping("/public/ticket/{id}")
    public ResponseEntity<TicketResponse> getTicketInfo(@PathVariable("id") Long ticketId) {
        return ResponseEntity.ok(ticketService.getShortTicketById(ticketId));
    }

    @PostMapping("/public/ticket/cancel/{id}")
    public ResponseEntity<String> cancelAppointmentById(@PathVariable("id") Long ticketId) {
        ticketService.cancelTicket(ticketId);
        return ResponseEntity.ok("Ticket cancelled.");
    }

    @PostMapping("/ticket/start/{id}")
    public ResponseEntity<String> startAppointmentById(@CurrentSecurityContext(expression = "authentication.userId")
                                                           @PathVariable("id") Long ticketId) {
        ticketService.markTicketHasStarted(ticketId);
        return ResponseEntity.ok("Appointment marked as started.");
    }

    @PostMapping("/ticket/end/{id}")
    public ResponseEntity<String> endAppointmentById(@CurrentSecurityContext(expression = "authentication.userId")
                                                         @PathVariable("id") Long ticketId) {
        ticketService.markTicketHasEnded(ticketId);
        return ResponseEntity.ok("Appointment marked as ended.");
    }

    @GetMapping("/tickets/specialist/{id}")
    public ResponseEntity<List<TicketResponse>> getSpecialistAppointmentsForToday(@CurrentSecurityContext(expression = "authentication.userId")
                                                                                      @PathVariable("id") Long specialistId) {
        return ResponseEntity.ok(ticketService.getTodayTicketsForOneSpecialist(specialistId));
    }

    @GetMapping("/tickets/current/specialist/{id}")
    public ResponseEntity<TicketResponse> getCurrentSpecialistAppointment(@CurrentSecurityContext(expression = "authentication.userId")
                                                                              @PathVariable("id") Long specialistId) {
        return ResponseEntity.ok(ticketService.getCurrentTicketForSpecialist(specialistId));
    }

    @GetMapping("/tickets/5/specialist/{id}")
    public ResponseEntity<List<TicketResponse>> getNextFiveAppointmentsForSpecialist(@CurrentSecurityContext(expression = "authentication.name")
                                                                                  @PathVariable("id") Long specialistId) {
        return ResponseEntity.ok(ticketService.getNextFiveTicketsForSpecialist(specialistId));
    }

}
