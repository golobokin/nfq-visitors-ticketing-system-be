package lt.nfq.intern.electronicvisitorsticketingsystem.controller;

import lombok.RequiredArgsConstructor;
import lt.nfq.intern.electronicvisitorsticketingsystem.payload.response.AvailableSpecialistsResponse;
import lt.nfq.intern.electronicvisitorsticketingsystem.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class UserController {
    private final UserService userService;

    @GetMapping("/public/specialists")
    public ResponseEntity<List<AvailableSpecialistsResponse>> getAvailableSpecialists() {
        return ResponseEntity.ok(userService.getAllAvailableSpecialists());
    }
}
