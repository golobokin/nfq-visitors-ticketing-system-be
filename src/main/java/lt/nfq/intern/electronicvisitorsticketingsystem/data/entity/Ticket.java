package lt.nfq.intern.electronicvisitorsticketingsystem.data.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@Table(name = "tickets", schema = "evts")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ticket_number")
    private Long ticketNumber;

    @Column(name = "creation_timestamp")
    @CreationTimestamp
    private LocalDateTime creationTimestamp;

    @Column(name = "visit_booked_time")
    private LocalDateTime visitBookedTime;

    @Column(name = "visit_start_time")
    private LocalDateTime visitStartTime;

    @Column(name = "visit_end_time")
    private LocalDateTime visitEndTime;

    @Column(name = "has_started")
    private boolean hasTicketStarted;

    @Column(name = "has_ended")
    private boolean hasTicketEnded;

    @Column(name = "is_cancelled")
    private boolean isCancelled;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @PrePersist
    private void setOnCreate() {
        this.hasTicketStarted = false;
        this.hasTicketEnded = false;
        this.isCancelled = false;
    }


}
