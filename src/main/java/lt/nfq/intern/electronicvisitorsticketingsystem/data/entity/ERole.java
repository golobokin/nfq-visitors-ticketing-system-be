package lt.nfq.intern.electronicvisitorsticketingsystem.data.entity;

public enum ERole {
    ROLE_ADMIN,
    ROLE_SPECIALIST
}
