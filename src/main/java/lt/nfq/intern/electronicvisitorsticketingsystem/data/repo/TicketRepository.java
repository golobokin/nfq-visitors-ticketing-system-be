package lt.nfq.intern.electronicvisitorsticketingsystem.data.repo;

import lt.nfq.intern.electronicvisitorsticketingsystem.data.entity.Ticket;
import lt.nfq.intern.electronicvisitorsticketingsystem.payload.response.TicketResponse;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket, Long> {
    TicketResponse findTicketById(Long id);

    List<TicketResponse> findAllByVisitBookedTimeBetweenAndUserId(LocalDateTime start, LocalDateTime finish, Long id);

    TicketResponse findTicketByVisitBookedTimeBetweenAndUserId(LocalDateTime start, LocalDateTime finish, Long id);

    List<TicketResponse> findFirst5ByVisitBookedTimeBetweenAndUserId(LocalDateTime start, LocalDateTime finish, Long id);
}
