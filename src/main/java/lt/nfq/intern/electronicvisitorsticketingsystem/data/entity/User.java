package lt.nfq.intern.electronicvisitorsticketingsystem.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@Entity
@Table(name = "users", schema = "evts")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String username;

    @JsonIgnore
    private String password;

    @Enumerated(value = EnumType.STRING)
    private ERole role;

    private String type;

    @Column(name = "shift_start_time")
    private LocalTime shiftStartTime;

    @Column(name = "shift_end_time")
    private LocalTime shiftEndTime;

    @Column(name = "visit_length_in_minutes")
    private Long visitLengthInMinutes;

    @Column(name = "next_ticket_time")
    private LocalDateTime nextTicketTime;

    @Column(name = "next_ticket_number")
    private Long nextTicketNumber;

}
