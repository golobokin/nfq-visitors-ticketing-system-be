package lt.nfq.intern.electronicvisitorsticketingsystem.data.repo;

import lt.nfq.intern.electronicvisitorsticketingsystem.data.entity.ERole;
import lt.nfq.intern.electronicvisitorsticketingsystem.data.entity.User;
import lt.nfq.intern.electronicvisitorsticketingsystem.payload.response.AvailableSpecialistsResponse;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Optional<User> findByIdAndRole(Long id, ERole role);

    List<AvailableSpecialistsResponse> findAllByRole(ERole role);

}
