package lt.nfq.intern.electronicvisitorsticketingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElectronicVisitorsTicketingSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElectronicVisitorsTicketingSystemApplication.class, args);
	}

}
