CREATE TABLE IF NOT EXISTS evts.users
(
    id                      SERIAL PRIMARY KEY,
    username                TEXT        NOT NULL,
    password                TEXT        NOT NULL,
    role                    TEXT        NOT NULL,
    type                    TEXT        NOT NULL,
    shift_start_time        TIME        NOT NULL,
    shift_end_time          TIME        NOT NULL,
    visit_length_in_minutes INTEGER     NOT NULL,
    next_ticket_time        TIMESTAMP   NOT NULL,
    next_ticket_number      INTEGER     NOT NULL
);
