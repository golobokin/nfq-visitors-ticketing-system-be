CREATE TABLE IF NOT EXISTS evts.tickets
(
    id                  SERIAL  PRIMARY KEY,
    ticket_number       INTEGER    NOT NULL,
    creation_timestamp  TIMESTAMP  NOT NULL,
    visit_booked_time   TIMESTAMP  NOT NULL,
    visit_start_time    TIMESTAMP,
    visit_end_time      TIMESTAMP,
    has_started         BOOLEAN    NOT NULL,
    has_ended           BOOLEAN    NOT NULL,
    is_cancelled        BOOLEAN    NOT NULL,
    user_id             INTEGER    NOT NULL,
    CONSTRAINT fk_tickets_users FOREIGN KEY (user_id) REFERENCES evts.users(id)
);
