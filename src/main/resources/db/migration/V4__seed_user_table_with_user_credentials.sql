INSERT INTO evts.users
(username, password, role, type, shift_start_time, shift_end_time, visit_length_in_minutes, next_ticket_time, next_ticket_number)
VALUES('admin', '$2y$12$1WhEK6UFcMpfj70rvtLe0eXKyhQyiOKp.s/LjS6AX2.l5N6qzLWii', 'ROLE_ADMIN', 'Administrator', '08:00:00', '17:00:00', 60, '2021-02-07 08:10:00', 0);
INSERT INTO evts.users
(username, password, role, type, shift_start_time, shift_end_time, visit_length_in_minutes, next_ticket_time, next_ticket_number)
VALUES('spec1', '$2y$12$ptIJnutwS6ixoB.bKIP/t.M/.dRWMzxAioSbWE8wTV9Xudd.JfzWm', 'ROLE_SPECIALIST', 'Consultant', '08:00:00', '14:00:00', 10, '2021-02-07 08:00:00', 1000);
INSERT INTO evts.users
(username, password, role, type, shift_start_time, shift_end_time, visit_length_in_minutes, next_ticket_time, next_ticket_number)
VALUES('spec2', '$2y$12$pTQ.S1iG/vVdotqTfmKuZOF9s6aySWyqb7tlT1bTG8yL3NiGYB9pO', 'ROLE_SPECIALIST', 'Loans', '14:00:00', '22:00:00', 10, '2021-02-07 08:00:00', 2000);
INSERT INTO evts.users
(username, password, role, type, shift_start_time, shift_end_time, visit_length_in_minutes, next_ticket_time, next_ticket_number)
VALUES('spec3', '$2y$12$ytK6.xsa.YDqUJichp1ukeXK0v1pbFTLX0oJGPATrEitPrSMMjcdi', 'ROLE_SPECIALIST', 'Real Estate', '08:00:00', '16:00:00', 15, '2021-02-07 08:00:00', 3000);
INSERT INTO evts.users
(username, password, role, type, shift_start_time, shift_end_time, visit_length_in_minutes, next_ticket_time, next_ticket_number)
VALUES('spec4', '$2y$12$sUCuWiI.s7GSmOCnCHQhHuI8Z7wQ9HOaMN1..2x5HYdWL44pq2Jcy', 'ROLE_SPECIALIST', 'Consultant', '14:00:00', '23:50:00', 10, '2021-02-07 08:00:00', 4000);
INSERT INTO evts.users
(username, password, role, type, shift_start_time, shift_end_time, visit_length_in_minutes, next_ticket_time, next_ticket_number)
VALUES('spec5', '$2y$12$J6HUE.Po17833FyoWmT1uuWBYGwdv/Tb0dB.WNyUhyOowYAJI9dx.', 'ROLE_SPECIALIST', 'Sleepy Support', '00:00:00', '09:00:00', 5, '2021-02-07 08:00:00', 5000);
INSERT INTO evts.users
(username, password, role, type, shift_start_time, shift_end_time, visit_length_in_minutes, next_ticket_time, next_ticket_number)
VALUES('spec6', '$2y$12$LXzKKq8ILwDP2PZfTpblp.0UUsxJkaaUCMabz9WNAU9vjAhl9yg0y', 'ROLE_SPECIALIST', 'Serviceman', '08:00:00', '17:00:00', 5, '2021-02-07 08:00:00', 6000);
