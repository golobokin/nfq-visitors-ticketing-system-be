--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: evts; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA evts;


ALTER SCHEMA evts OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: flyway_schema_history; Type: TABLE; Schema: evts; Owner: postgres
--

CREATE TABLE evts.flyway_schema_history (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE evts.flyway_schema_history OWNER TO postgres;

--
-- Name: tickets; Type: TABLE; Schema: evts; Owner: postgres
--

CREATE TABLE evts.tickets (
    id integer NOT NULL,
    ticket_number integer NOT NULL,
    creation_timestamp timestamp without time zone NOT NULL,
    visit_booked_time timestamp without time zone NOT NULL,
    visit_start_time timestamp without time zone,
    visit_end_time timestamp without time zone,
    has_started boolean NOT NULL,
    has_ended boolean NOT NULL,
    is_cancelled boolean NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE evts.tickets OWNER TO postgres;

--
-- Name: tickets_id_seq; Type: SEQUENCE; Schema: evts; Owner: postgres
--

CREATE SEQUENCE evts.tickets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE evts.tickets_id_seq OWNER TO postgres;

--
-- Name: tickets_id_seq; Type: SEQUENCE OWNED BY; Schema: evts; Owner: postgres
--

ALTER SEQUENCE evts.tickets_id_seq OWNED BY evts.tickets.id;


--
-- Name: users; Type: TABLE; Schema: evts; Owner: postgres
--

CREATE TABLE evts.users (
    id integer NOT NULL,
    username text NOT NULL,
    password text NOT NULL,
    role text NOT NULL,
    type text NOT NULL,
    shift_start_time time without time zone NOT NULL,
    shift_end_time time without time zone NOT NULL,
    visit_length_in_minutes integer NOT NULL,
    next_ticket_time timestamp without time zone NOT NULL,
    next_ticket_number integer NOT NULL
);


ALTER TABLE evts.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: evts; Owner: postgres
--

CREATE SEQUENCE evts.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE evts.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: evts; Owner: postgres
--

ALTER SEQUENCE evts.users_id_seq OWNED BY evts.users.id;


--
-- Name: tickets id; Type: DEFAULT; Schema: evts; Owner: postgres
--

ALTER TABLE ONLY evts.tickets ALTER COLUMN id SET DEFAULT nextval('evts.tickets_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: evts; Owner: postgres
--

ALTER TABLE ONLY evts.users ALTER COLUMN id SET DEFAULT nextval('evts.users_id_seq'::regclass);


--
-- Data for Name: flyway_schema_history; Type: TABLE DATA; Schema: evts; Owner: postgres
--

COPY evts.flyway_schema_history (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
0	\N	<< Flyway Schema Creation >>	SCHEMA	"evts"	\N	postgres	2021-02-11 17:05:47.343222	0	t
1	1	initial schema	SQL	V1__initial_schema.sql	-1810997062	postgres	2021-02-11 17:05:47.368224	7	t
2	2	create user table	SQL	V2__create_user_table.sql	1969165962	postgres	2021-02-11 17:05:47.387225	11	t
3	3	create ticket table	SQL	V3__create_ticket_table.sql	1371940933	postgres	2021-02-11 17:05:47.410226	7	t
4	4	seed user table with user credentials	SQL	V4__seed_user_table_with_user_credentials.sql	983619158	postgres	2021-02-11 17:05:47.428227	5	t
\.


--
-- Data for Name: tickets; Type: TABLE DATA; Schema: evts; Owner: postgres
--

COPY evts.tickets (id, ticket_number, creation_timestamp, visit_booked_time, visit_start_time, visit_end_time, has_started, has_ended, is_cancelled, user_id) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: evts; Owner: postgres
--

COPY evts.users (id, username, password, role, type, shift_start_time, shift_end_time, visit_length_in_minutes, next_ticket_time, next_ticket_number) FROM stdin;
1	admin	$2y$12$1WhEK6UFcMpfj70rvtLe0eXKyhQyiOKp.s/LjS6AX2.l5N6qzLWii	ROLE_ADMIN	Administrator	08:00:00	17:00:00	60	2021-02-07 08:10:00	0
2	spec1	$2y$12$ptIJnutwS6ixoB.bKIP/t.M/.dRWMzxAioSbWE8wTV9Xudd.JfzWm	ROLE_SPECIALIST	Consultant	08:00:00	14:00:00	10	2021-02-07 08:00:00	1000
4	spec3	$2y$12$ytK6.xsa.YDqUJichp1ukeXK0v1pbFTLX0oJGPATrEitPrSMMjcdi	ROLE_SPECIALIST	Real Estate	08:00:00	16:00:00	15	2021-02-07 08:00:00	3000
6	spec5	$2y$12$J6HUE.Po17833FyoWmT1uuWBYGwdv/Tb0dB.WNyUhyOowYAJI9dx.	ROLE_SPECIALIST	Sleepy Support	00:00:00	09:00:00	5	2021-02-07 08:00:00	5000
7	spec6	$2y$12$LXzKKq8ILwDP2PZfTpblp.0UUsxJkaaUCMabz9WNAU9vjAhl9yg0y	ROLE_SPECIALIST	Serviceman	08:00:00	17:00:00	5	2021-02-07 08:00:00	6000
3	spec2	$2y$12$pTQ.S1iG/vVdotqTfmKuZOF9s6aySWyqb7tlT1bTG8yL3NiGYB9pO	ROLE_SPECIALIST	Loans	14:00:00	22:00:00	10	2021-02-11 17:17:13.540153	3001
5	spec4	$2y$12$sUCuWiI.s7GSmOCnCHQhHuI8Z7wQ9HOaMN1..2x5HYdWL44pq2Jcy	ROLE_SPECIALIST	Consultant	14:00:00	23:50:00	10	2021-02-11 20:14:00.283379	5006
\.


--
-- Name: tickets_id_seq; Type: SEQUENCE SET; Schema: evts; Owner: postgres
--

SELECT pg_catalog.setval('evts.tickets_id_seq', 7, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: evts; Owner: postgres
--

SELECT pg_catalog.setval('evts.users_id_seq', 7, true);


--
-- Name: flyway_schema_history flyway_schema_history_pk; Type: CONSTRAINT; Schema: evts; Owner: postgres
--

ALTER TABLE ONLY evts.flyway_schema_history
    ADD CONSTRAINT flyway_schema_history_pk PRIMARY KEY (installed_rank);


--
-- Name: tickets tickets_pkey; Type: CONSTRAINT; Schema: evts; Owner: postgres
--

ALTER TABLE ONLY evts.tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: evts; Owner: postgres
--

ALTER TABLE ONLY evts.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: flyway_schema_history_s_idx; Type: INDEX; Schema: evts; Owner: postgres
--

CREATE INDEX flyway_schema_history_s_idx ON evts.flyway_schema_history USING btree (success);


--
-- Name: tickets fk_tickets_users; Type: FK CONSTRAINT; Schema: evts; Owner: postgres
--

ALTER TABLE ONLY evts.tickets
    ADD CONSTRAINT fk_tickets_users FOREIGN KEY (user_id) REFERENCES evts.users(id);


--
-- PostgreSQL database dump complete
--

