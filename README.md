# Electronic Visitor Ticketing System 
*Electronic Visitor Ticketing System* is a web based application where Customers can book a visit with a Specialist in real time, replacing standard paper-based ticket solutions. 


## Table of contents
* [Setup](#setup)
* [Features](#features)
* [Technologies](#technologies)


## Setup
#### 1. Necessary resources
1. Install [node.js](https://nodejs.org/en/) to be able to use npm commands.
2. For proper app functionality you also need to clone and configure backend server from [this repository](https://bitbucket.org/golobokin/nfq-visitors-ticketing-system-be).

#### 2. Starting application

* Clone this repository
`$ git clone https://bitbucket.org/golobokin/nfq-visitors-ticketing-system-be.git`

* Install [PostgreSQL](https://www.postgresql.org) database
* In project folder `src/main/resources` open `application.properties` file. Check if spring.datasource.url port is same as when defined during installation of PostgreSQL (default port - 5432).
* In `application.properties` file configure your database username (default: postgres) and password
* Run main from ElectronicVisitorsTicketingSystemApplication
* DB schema will be automatically initiated by Flyway migrations
* Open [http://localhost:8080](http://localhost:8080) in your browser for a welcome message. Your backend is up and running.
* Clone [this frontend repository](https://bitbucket.org/golobokin/nfq-visitors-ticketing-system-fe) and setup according to instructions for full experience.


## Features 
#### **Demo version**
Access live demo of this app here:

* [CUSTOMER URL](https://evts.vercel.app/customer)
* [INTERNAL USAGE URL](https://evts.vercel.app/specialist) (specialist / admin logins)

*see below tables for available users and credentials*

##### **Customer**
* As a new Customer we can access Customer board, click ‘Reserve a ticket’ and select a specialist we want to visit (on this screen Specialists are filtered by valid shift time). 
* By clicking on a specialist a ticket is provided and Customer is redirected to the ticket page, where he can see the ticket number, booked time, countdown until visit. 
* From this screen the Customer can also cancel his ticket. 
* While the ticket is not cancelled, Customer cannot reserve a new one and can access his ticket from the Customer board.


##### **Specialist**
* Specialists can login with their credentials. 
* ‘Your next appointments’ screen shows a board with current appointment and all upcoming appointments of Customers which are planned for his shift and have registered with this specialist. 
* When the booked time has started, ticket becomes current and specialist can either mark the start of appointment or cancel ticket (provided the Customer did not cancel it earlier). 
* After marking the start of the appointment he can mark the end (start and end times are marked in db but not used for functionality in app).
* Screen data is updated every five seconds.

##### **Ticket**
* Tickets are issued based on specialist visit length and visit time is calculated by system. 
* If a ticket is cancelled, it does not affect other tickets and specialist can have a coffee break:)
* Tickets can be issued with appointment start time within a certain specialists shift time.
* Ticket numbers are reset every day.
* One Customer can issue one ticket at a time. If the ticket is cancelled he can issue another one. 

##### **Department screen**
* Can be accessed only by user with administrator rights. 
* Department screen shows the data for specialists who are on shift now. 
* Screen shows current appointment ticket number, time left until next visit and 5 upcoming tickets.
* Screen data is updated every five seconds.

##### **Logging in**
* Specialist should login with his credentials (see table below)
* Department screen can be accessed with administrator credentials (see second table below)

##### *Currently available specialists (seeded straight to db) - distincted in app by type:*

*type* | *shift time* | *visit length* | *username* | *password*
--- | --- | --- | --- | ---
Consultant | 08:00-14:00 | 10 mins | spec1 | spec1
Loans | 14:00-22:00 | 10 mins | spec2 | spec2
Real Estate | 08:00-16:00 | 15 mins | spec3 | spec3
Consultant | 14:00-23:50 | 10 mins | spec4 | spec4
Sleepy Support | 00:00-09:00 | 5 mins | spec5 | spec5
Serviceman | 08:00-17:00 | 5 mins | spec6 | spec6

##### *Also Administrator to see Department Screen* 

*type* | *shift time* | *visit length* | *username* | *password*
--- | --- | --- | --- | ---
Administrator |  |  | admin | admin



## Technologies
* Java 11
* Spring Boot
* Spring Security
* Gradle 6.7.1
* PostgreSQL
* Flyway
